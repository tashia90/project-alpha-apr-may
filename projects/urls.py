from django.urls import path

from projects.views import (
    ProjectListView,
    ProjectCreateView,
)

urlpatterns = [
    path("", ProjectListView.as_view(), name="home"),
    path("accounts/", ProjectListView.as_view(), name="list_projects"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
